import jason.environment.grid.Location;
import util.LocationUtils;

import java.rmi.MarshalException;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractAgent {
    private Location currentLocation;
    private int agNum;
    private Location currentGoal;
    private LinkedList<Location> path;

    public AbstractAgent(int agNum) {
        this.agNum = agNum;
        path = new LinkedList<>();
    }

    public int getAgNum() {
        return agNum;
    }

    public void setAgNum(int agNum) {
        this.agNum = agNum;
    }

    public Location getCurrentGoal() {
        return currentGoal;
    }

    public void setCurrentGoal(Location currentGoal) {
        this.currentGoal = currentGoal;

    }

    public List<Location> getPath() {
        return path;
    }

    public void addToPath(Location location) {
        path.add(location);
    }

    public boolean pathContainsLocation(Location location) {
        for (Location pathElement :
                path) {
            if (location.equals(pathElement))
                return true;
        }
        return false;
    }

    public Location getNextLocation() {
        if (!path.isEmpty()) {
            return path.removeFirst();
        } else {
            return null;
        }
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }
}
