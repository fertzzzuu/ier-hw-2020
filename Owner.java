import jason.environment.grid.Location;

public class Owner extends AbstractAgent {
    private Room currentRoom;

    Owner(int agNum, Room currentRoom) {
        super(agNum);
        this.currentRoom = currentRoom;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        if (this.currentRoom != currentRoom) {
            if (this.currentRoom != null) {
                this.currentRoom.setOccupied(false);
                this.currentRoom.setDirty(true);
            }
            this.currentRoom = currentRoom;
            currentRoom.setOccupied(true);
        }
    }

    public boolean isLeft() {
        return getCurrentLocation().equals(SmartHomeModel.entrance);
    }

}
