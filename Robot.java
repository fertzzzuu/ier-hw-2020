import jason.environment.grid.Location;

import java.util.List;

public class Robot extends AbstractAgent{
    private boolean busy;

    public Robot(int agNum) {
        super(agNum);
    }

    public void startCleaning(Room room) throws InterruptedException {
        this.busy = true;
        Thread.sleep(room.getTimeToClean());
        room.setDirty(false);
        this.busy = true;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

}
