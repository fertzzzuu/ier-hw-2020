import jason.environment.grid.Location;
import jason.util.Pair;

import java.util.List;
import java.util.Random;

public class Room {
    private Pair<Location, Location> borders;
    private boolean occupied;
    private boolean dirty = false;
    private List<Location> doors;

    public void setBorders(Location from, Location to) {
        this.borders = new Pair<>(from, to);
    }

    public boolean contains(Location location) {
        return location.x >= borders.getFirst().x &&
                location.x <= borders.getSecond().x &&
                location.y >= borders.getFirst().y &&
                location.y <= borders.getSecond().y;
    }

    public Location getCenter() {
        return new Location((borders.getFirst().x + borders.getSecond().x)/2,
                (borders.getFirst().y + borders.getSecond().y)/2);
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public int getTimeToClean() {
        return (new Random().nextInt(5) + 5) * 1000;
    }

    public List<Location> getDoors() {
        return doors;
    }

    public void setDoors(List<Location> doors) {
        this.doors = doors;
    }
}