// Environment code for project smart_home.mas2j

import jason.asSyntax.*;
import jason.environment.*;
import java.util.logging.*;

public class SmartHomeEnv extends Environment {

    private Logger logger = Logger.getLogger("smart_home.mas2j."+SmartHomeEnv.class.getName());
    private SmartHomeModel model;
    private SmartHomeView view;

    /** Called before the MAS execution with the args informed in .mas2j */
    @Override
    public void init(String[] args) {
        super.init(args);
        model = new SmartHomeModel();
        view = new SmartHomeView(model, "smart-home", 600);
        model.setView(view);
        updatePercepts();
        //addPercept(ASSyntax.parseLiteral("percept(demo)"));
    }

    private void updatePercepts() {
        if (model.isOwnerLeft()) {
            logger.info("Owner left adding ownerLeft percept");
            addPercept("robotka", Literal.parseLiteral("ownerLeft"));
        } else {
            logger.info("Owner came back removing ownerLeft percept");
            removePercept("robotka", Literal.parseLiteral("ownerLeft"));
        }

    }

    @Override
    public boolean executeAction(String agName, Structure action) {
        logger.info(agName + " doing: " + action);
        boolean result = true;
        if (action.equals(Literal.parseLiteral("move(owner)"))){
            result = model.moveOwner();
        } else if (action.equals(Literal.parseLiteral("move(robotka)"))) {
            result = model.moveRobotka();
        }

        updatePercepts();
        informAgsEnvironmentChanged();
        return result; // the action was executed with success
    }

    /** Called before the end of MAS execution */
    @Override
    public void stop() {
        super.stop();
    }
}

