import jason.environment.grid.GridWorldModel;
import jason.environment.grid.Location;
import util.LocationUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

public class SmartHomeModel extends GridWorldModel {
    private final Logger logger = Logger.getLogger(String.valueOf(SmartHomeModel.class));

    //Objects
    private final Robot robot;
    private final Owner owner;

    private final static int gSize = 13;

    final static Location entrance = new Location(6, 12);
    private final Room[][] rooms = new Room[][]{
            {new Room(), new Room(), new Room()},
            {new Room(), new Room(), new Room()},
            {new Room(), new Room(), new Room()}
    };

    public SmartHomeModel() {
        super(gSize, gSize, 2);
        owner = new Owner(1, null);
        owner.setCurrentLocation(new Location(6, 2));
        owner.setCurrentGoal(new Location(6, 12));
        calculatePath(owner, owner.getCurrentLocation());
        robot = new Robot(0);

        setAgPos(robot.getAgNum(), 2, 2);
        setAgPos(owner.getAgNum(), owner.getCurrentLocation().x, owner.getCurrentLocation().y);

        addWalls();
        setRoomBorders();
    }

    private void addWalls() {
        addWall(0, 0, gSize - 1, 0);
        addWall(0, 0, 0, gSize - 1);
        addWall(gSize - 1, 0, gSize - 1, gSize - 1);
        addWall(0, gSize - 1, 5, gSize - 1);
        addWall(7, gSize - 1, gSize - 1, gSize - 1);
        addWall(4, 1, 4, 1);
        addWall(8, 1, 8, 1);
        addWall(11, 4, 11, 4);
        addWall(4, 11, 4, 11);
        addWall(8, 11, 8, 11);
        addWall(11, 8, 11, 8);
        addWall(1, 8, 1, 8);
        addWall(1, 4, 1, 4);
        addWall(3, 4, 5, 4);
        addWall(7, 4, 9, 4);
        addWall(3, 8, 5, 8);
        addWall(7, 8, 9, 8);
        addWall(4, 3, 4, 5);
        addWall(4, 7, 4, 9);
        addWall(8, 3, 8, 5);
        addWall(8, 3, 8, 5);
        addWall(8, 7, 8, 9);
    }

    private void setRoomBorders() {
        rooms[0][0].setBorders(new Location(1, 1), new Location(3, 3));
        rooms[0][1].setBorders(new Location(5, 1), new Location(7, 3));
        rooms[0][2].setBorders(new Location(9, 1), new Location(11, 3));
        rooms[1][0].setBorders(new Location(1, 5), new Location(3, 7));
        rooms[1][1].setBorders(new Location(5, 5), new Location(7, 7));
        rooms[1][2].setBorders(new Location(9, 5), new Location(11, 7));
        rooms[2][0].setBorders(new Location(1, 9), new Location(3, 11));
        rooms[2][1].setBorders(new Location(5, 9), new Location(7, 11));
        rooms[2][2].setBorders(new Location(9, 9), new Location(11, 11));
    }

    boolean moveOwner() {
        Location goal = owner.getNextLocation();
        if (goal != null) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (rooms[i][j].contains(goal)) {
                        logger.info("owner is in room: " + i + ", " + j);
                        owner.setCurrentRoom(rooms[i][j]);
                    }
                }
            }
            logger.info("moving owner to " + goal.toString());
            setAgPos(owner.getAgNum(), goal);
            owner.setCurrentLocation(goal);
        }
        return true;
    }

    boolean moveRobotka() {
        if (robot.getCurrentGoal() == null) { // TODO: 2020. 05. 25. this if can be replaced with a good perception
            logger.info("Robot current goal is null");
            List<Room> dirtyRooms = getDirtyRooms();
            Location robotLocation = getAgPos(robot.getAgNum());

            Room closest = getClosestDirtyRoom(robotLocation, dirtyRooms);
            if (closest != null && !closest.isOccupied()) {
                robot.setCurrentGoal(closest.getCenter());
                calculatePath(robot, robotLocation);
            } else {
                logger.info("no dirty rooms");
            }
        } else {
            logger.info(String.format("robot.currentGoal: %d, %d", robot.getCurrentGoal().x, robot.getCurrentGoal().y));
            Location nextLocation = robot.getNextLocation();
            if (nextLocation != null) {
                setAgPos(robot.getAgNum(), nextLocation);
                robot.setCurrentLocation(nextLocation);
            } else { //todo add at perception to know we reached the goal
                Room roomFromLocation = getRoomFromLocation(robot.getCurrentLocation());
                if (roomFromLocation != null) {
                    try {
                        robot.startCleaning(roomFromLocation);
                    } catch (InterruptedException e) {
                        logger.info("Cleaning was interrupted.");
                        return false;
                    }
                }
                robot.setCurrentGoal(null);
            }
        }
        return true;
    }

    private Room getRoomFromLocation(Location location) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (rooms[i][j].contains(location))
                    return rooms[i][j];
            }
        }
        return null;
    }

    private Room getClosestDirtyRoom(Location robotLocation, List<Room> dirtyRooms) {
        if (dirtyRooms == null || dirtyRooms.isEmpty())
            return null;

        Room closest = dirtyRooms.get(0);
        for (Room room : dirtyRooms) {
            if (robotLocation.distance(room.getCenter()) < robotLocation.distance(closest.getCenter()))
                closest = room;
        }
        logger.info(String.format("Closest dirty room is at (%d, %d)", closest.getCenter().x, closest.getCenter().y));
        return closest;
    }

    private List<Room> getDirtyRooms() {
        List<Room> dirtyRooms = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (rooms[i][j].isDirty()) {
                    logger.info(String.format("Found dirty room: [%d][%d]", i, j));
                    dirtyRooms.add(rooms[i][j]);
                }
            }
        }
        return dirtyRooms;
    }

    public boolean isOwnerLeft() {
        return owner.isLeft();
    }


    private void calculatePath(AbstractAgent agent, Location start) {
        if (start.equals(agent.getCurrentGoal()))
            return;

        Location dirV = new Location(agent.getCurrentGoal().x - start.x, agent.getCurrentGoal().y - start.y);
        Location eV = LocationUtils.normalizeLocation(dirV);

        if (!isAvailable(start, eV, agent))
            LocationUtils.swapCoordinates(eV);
        if (!isAvailable(start, eV, agent))
            LocationUtils.negateCoordinates(eV);
        if (!isAvailable(start, eV, agent))
            LocationUtils.swapCoordinates(eV);
        if (!isAvailable(start, eV, agent))
            return;

        Location step = new Location(start.x + eV.x, start.y + eV.y);
        agent.addToPath(step);
        calculatePath(agent, step);
    }

    private boolean isAvailable(Location start, Location eV, AbstractAgent agent) {
        Location step = new Location(start.x + eV.x, start.y + eV.y);
        return (isFreeOfObstacle(step) && !agent.pathContainsLocation(step));
    }
}