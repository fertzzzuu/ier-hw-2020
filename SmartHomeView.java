import jason.environment.grid.GridWorldModel;
import jason.environment.grid.GridWorldView;

import java.awt.*;

public class SmartHomeView extends GridWorldView {
    public SmartHomeView(GridWorldModel model, String title, int windowSize) {
        super(model, title, windowSize);
        setResizable(false);
        setVisible(true);
        repaint();
    }

    @Override
    public void drawAgent(Graphics g, int x, int y, Color c, int id) {
        super.drawAgent(g, x, y, c, id);
    }
}
