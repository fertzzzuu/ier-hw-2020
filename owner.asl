// Agent owner in project smart_home.mas2j

/* Initial beliefs and rules */

/* Initial goals */

!move.

/* Plans */

+!move : true <-
	.wait(1000)
	move(owner);
	!move.

