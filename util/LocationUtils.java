package util;

import jason.environment.grid.Location;

public class LocationUtils {
    public static Location normalizeLocation(Location location){
        if (location.x > location.y) {
            return new Location(location.x / Math.abs(location.x), 0);
        } else {
            return new Location(0, location.y / Math.abs(location.y));
        }
    }

    public static void swapCoordinates(Location location){
        int tmp = location.x;
        location.x = location.y;
        location.y = tmp;
    }

    public static void negateCoordinates(Location location) {
        location.x = Math.negateExact(location.x);
        location.y = Math.negateExact(location.y);
    }
}
